package torneo;

import junit.framework.TestCase;

public class TorneoTest extends TestCase {
	public void testTorneo() {

		Torneo torneo = new Torneo();
		torneo.id = 1;
		
		Equipo equipo1 = new Equipo();
		equipo1.nombre = "Equipo 1";
		equipo1.id = 1;
		Equipo equipo2 = new Equipo();
		equipo2.nombre = "Equipo 2";
		equipo2.id = 2;
		Equipo equipo3 = new Equipo();
		equipo3.nombre = "Equipo 3";
		equipo3.id = 3;
		Equipo equipo4= new Equipo();
		equipo4.nombre = "Equipo 4"; 
		equipo4.id = 4;
		
		Persona persona1 = new Persona();
		persona1.nombre = "Julio";
		persona1.apellidos = "Martinez Perez";
		persona1.ciudad = "Murcia";
		persona1.cp = 03002;
		persona1.equipo_id = equipo1;
		persona1.direccion = "AV/ Juan Carlos I";
		persona1.pais = "Espania";
		persona1.telefono = "692837423";
		
		Persona persona2 = new Persona();
		persona2.nombre = "Andrez";
		persona2.apellidos = "Ramirez Uriol";
		persona2.ciudad = "Murcia";
		persona2.cp = 03002;
		persona2.equipo_id = equipo1;
		persona1.direccion = "AV/ Juan Carlos I";
		persona1.pais = "Espania";
		persona1.telefono = "684126532";

		Persona persona3 = new Persona();
		persona3.nombre = "Pedro";
		persona3.apellidos = "Diaz Alcantara";
		persona3.ciudad = "Murcia";
		persona3.cp = 03002;
		persona3.equipo_id = equipo2;
		persona1.direccion = "AV/ Juan Carlos I";
		persona1.pais = "Espania";
		persona1.telefono = "687644345";
		
		Persona persona4 = new Persona();
		persona4.nombre = "Jose";
		persona4.apellidos = "Fernandez Garcia";
		persona4.ciudad = "Murcia";
		persona4.cp = 03002;
		persona4.equipo_id = equipo2;
		persona1.direccion = "AV/ Juan Carlos I";
		persona1.pais = "Espania";
		persona1.telefono = "69432103292";
	
		Persona persona5 = new Persona();
		persona5.nombre = "Alvaro";
		persona5.apellidos = "Rulo Alvarez";
		persona5.ciudad = "Murcia";
		persona5.cp = 03002;
		persona5.equipo_id = equipo3;
		persona1.direccion = "AV/ Juan Carlos I";
		persona1.pais = "Espania";
		persona1.telefono = "694121393";
		
		Persona persona6 = new Persona();
		persona6.nombre = "Santi";
		persona6.apellidos = "Dominguez Castillo";
		persona6.ciudad = "Murcia";
		persona6.cp = 03002;
		persona6.equipo_id = equipo3;
		persona1.direccion = "AV/ Juan Carlos I";
		persona1.pais = "Espania";
		persona1.telefono = "614124639";
		
		Persona persona7 = new Persona();
		persona7.nombre = "Tomas";
		persona7.apellidos = "Muros Castaniedo";
		persona7.ciudad = "Murcia";
		persona7.cp = 03002;
		persona7.equipo_id = equipo4;
		persona1.direccion = "AV/ Juan Carlos I";
		persona1.pais = "Espania";
		persona1.telefono = "674924122";
		
		Persona persona8 = new Persona();
		persona8.nombre = "Francisco";
		persona8.apellidos = "Sabater Pinto";
		persona8.ciudad = "Murcia";
		persona8.cp = 03002;
		persona8.equipo_id = equipo4;
		persona8.direccion = "AV/ Juan Carlos I";
		persona8.pais = "Espania";
		persona8.telefono = "624324426";
		
		
		torneo.equipos = new Equipo[4];
		torneo.equipos[0] = equipo1;
		torneo.equipos[1] = equipo2;
		torneo.equipos[2] = equipo3;
		torneo.equipos[3] = equipo4;
		

		assertTrue(torneo.equipos[0] == equipo1);
		assertTrue(torneo.equipos[1] == equipo2);
		assertTrue(torneo.equipos[2] == equipo3);
		assertTrue(torneo.equipos[3] == equipo4);
		
		
		
	}

}
